# Boards & Notes Manager, based on: FastAPI, SQLAlchemy, AsyncPG.

## First steps.
Create two files with using examples:

    .postgres.env

and

    .backend.env

## How to build.
Run:

    docker-compose build

## How to run.
    
    docker-compose up
or

    docker-compose up -d

## Apply Migrations.
Run:
    
    docker exec -it notes_manager_web_1 alembic upgrade head

to apply initial migrations which will init Alembic and will create Boards and Notes tables.

## Documentation.

    swagger - http://localhost:8000/docs
    redoc - http://localhost:8000/redoc

## Pgadmin4.

    http://localhost:5050