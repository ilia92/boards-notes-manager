# Pull base image
FROM python:3.10.10-slim-buster

# Set environment varibles
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

WORKDIR /src/

# Install dependencies
COPY requirements.txt /app/requirements.txt
RUN pip install --upgrade pip==21.2.4 \
    && pip install -r /app/requirements.txt

COPY ./src/ /src/

EXPOSE 8000