from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from core.db import get_session
from schema.board_schema import BoardSchema
from services.board_service import BoardService


router = APIRouter()


class BoardRoutes:
    def __init__(self):
        self.router = APIRouter()
        self.board_service = BoardService()

        self.router.get("/boards/{board_id}", response_model=BoardSchema)(
            self.get_board
        )
        self.router.get("/boards", response_model=list[BoardSchema])(
            self.get_boards
        )
        self.router.post("/boards", response_model=BoardSchema)(
            self.create_board
        )
        self.router.patch("/boards/{board_id}", response_model=BoardSchema)(
            self.update_board
        )
        self.router.post("/boards/delete/{board_id}")(self.delete_board)

    async def get_board(self, board_id: int, session: AsyncSession = Depends(get_session)):
        return await self.board_service.get_board_by_id(session, board_id)

    async def get_boards(self, session: AsyncSession = Depends(get_session)):
        return await self.board_service.get_all_boards(session)

    async def create_board(self, board: dict, session: AsyncSession = Depends(get_session)):
        res = await self.board_service.create_board(session, board)
        return res

    async def update_board(self, board_id: int, board_data: dict, session: AsyncSession = Depends(get_session)):
        res = await self.board_service.update_board(session, board_id, board_data)
        return res

    async def delete_board(self, board_id: int, session: AsyncSession = Depends(get_session)):
        res = await self.board_service.delete_board(session, board_id)
        return res
