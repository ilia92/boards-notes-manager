from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession

from core.db import get_session
from schema.note_schema import NoteSchema
from services.note_service import NoteService


router = APIRouter()


class NoteRoutes:
    def __init__(self):
        self.router = APIRouter()
        self.note_service = NoteService()

        self.router.get("/notes/{note_id}", response_model=NoteSchema)(
            self.get_note
        )
        self.router.get("/notes", response_model=list[NoteSchema])(
            self.get_notes
        )
        self.router.post("/notes", response_model=NoteSchema)(
            self.create_note
        )
        self.router.patch("/notes/{note_id}", response_model=NoteSchema)(
            self.update_note
        )
        self.router.post("/notes/delete/{note_id}")(self.delete_note)

    async def get_note(self, note_id: int, session: AsyncSession = Depends(get_session)):
        return await self.note_service.get_note_by_id(session, note_id)

    async def get_notes(self, session: AsyncSession = Depends(get_session)):
        return await self.note_service.get_all_notes(session)

    async def create_note(self, note: dict, session: AsyncSession = Depends(get_session)):
        res = await self.note_service.create_note(session, note)
        return res

    async def update_note(self, note_id: int, note_data: dict, session: AsyncSession = Depends(get_session)):
        res = await self.note_service.update_note(session, note_id, note_data)
        return res

    async def delete_note(self, note_id: int, session: AsyncSession = Depends(get_session)):
        res = await self.note_service.delete_note(session, note_id)
        return res
