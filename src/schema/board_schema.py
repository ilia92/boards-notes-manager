from pydantic.v1 import BaseModel

from schema.base_schema import BaseSchema


class BoardSchema(BaseSchema):
    name: str


class BoardCreateSchema(BaseModel):
    name: str
