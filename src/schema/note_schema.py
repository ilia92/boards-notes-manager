from typing import Optional

from schema.base_schema import BaseSchema


class NoteSchema(BaseSchema):
    text: str
    seen: int
    board_id: Optional[int] = None
