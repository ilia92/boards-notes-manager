"""add_note_and_board_tables

Revision ID: 22ab67eb152d
Revises: 
Create Date: 2023-09-12 08:13:41.794895

"""
from sqlalchemy import func

from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '22ab67eb152d'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'boards',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(timezone=True), server_default=func.now(), nullable=False),
        sa.Column('updated_at', sa.DateTime(timezone=True), nullable=True, server_default=func.now(), onupdate=func.now()),
        sa.PrimaryKeyConstraint('id')
    )


    op.create_table(
        'notes',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('text', sa.String(), nullable=False),
        sa.Column('created_at', sa.DateTime(timezone=True), server_default=func.now(), nullable=False),
        sa.Column('updated_at', sa.DateTime(timezone=True), server_default=func.now(), onupdate=func.now(), nullable=False),
        sa.Column('seen', sa.Integer(), default=0),
        sa.Column('board_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['board_id'], ['boards.id']),
        sa.PrimaryKeyConstraint('id')
    )


def downgrade():
    op.drop_table('boards')
    op.drop_table('notes')