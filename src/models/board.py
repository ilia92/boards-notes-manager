from sqlalchemy import Column, String

from models.base_model import BaseModel


class Board(BaseModel):
    __tablename__ = 'boards'

    name = Column(String, nullable=False)
