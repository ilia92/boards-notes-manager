from sqlalchemy import Column, Integer, DateTime, sql

from core.db import Base


class BaseModel(Base):
    __abstract__ = True

    id = Column(Integer, primary_key=True)
    created_at = Column(
        DateTime(timezone=True),
        server_default=sql.func.now(),
        nullable=False
    )
    updated_at = Column(
        DateTime(timezone=True),
        server_default=sql.func.now(),
        onupdate=sql.func.now(),
        nullable=False
    )
