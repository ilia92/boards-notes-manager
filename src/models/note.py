from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

from models.base_model import BaseModel


class Note(BaseModel):
    __tablename__ = "notes"

    text = Column(String, nullable=False)
    seen = Column(Integer, default=0)
    board_id = Column(Integer, ForeignKey('boards.id'), nullable=True)

    board = relationship('Board', backref="notes")
