from repo.board_repo import BoardRepo


class BoardService:
    def __init__(self):
        self.repo = BoardRepo()

    async def get_board_by_id(self, session, board_id):
        return await self.repo.get_by_id(session, board_id)

    async def get_all_boards(self, session):
        return await self.repo.get_all(session)

    async def create_board(self, session, board):
        return await self.repo.create(session, board)

    async def update_board(self, session, board_id, board_data):
        return await self.repo.update(session, board_id, board_data)

    async def delete_board(self, session, board_id):
        return await self.repo.delete(session, board_id)
