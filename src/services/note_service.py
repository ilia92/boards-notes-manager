from repo.board_repo import BoardRepo
from repo.note_repo import NoteRepo


class NoteService:
    def __init__(self):
        self.repo = NoteRepo()
        self.board_repo = BoardRepo()

    async def check_if_board_exists(self, session, board_id: int) -> bool:
        board = await self.board_repo.get_by_id(session, board_id)
        return board

    async def get_note_by_id(self, session, note_id: int):
        return await self.repo.get_by_id(session, note_id)


    async def get_all_notes(self, session):
        return await self.repo.get_all(session)

    async def create_note(self, session, note: dict):
        return await self.repo.create(session, note)

    async def update_note(self, session, note_id: int, note_data: dict):
        board_id = note_data.get('board_id')
        if board_id:
            await self.check_if_board_exists(session, board_id)
        return await self.repo.update(session, note_id, note_data)

    async def delete_note(self, session, note_id: int):
        return await self.repo.delete(session, note_id)
