from fastapi import FastAPI

from routes.board_routes import BoardRoutes
from routes.note_routes import NoteRoutes

app = FastAPI()


board_routes = BoardRoutes()
note_routes = NoteRoutes()

app.include_router(board_routes.router, tags=["boards"])
app.include_router(note_routes.router, tags=["notes"])
