from abc import ABC
from fastapi import HTTPException

from sqlalchemy import select

from models.board import Board
from repo.base_repo import BaseRepo


class BoardRepo(BaseRepo, ABC):
    async def get_by_id(self, session, board_id: int):
        board = await session.execute(
            select(Board).filter(
                Board.id == board_id
            )
        )
        board = board.scalars().first()

        if not board:
            raise HTTPException(status_code=404, detail="Board not found")
        return board


    async def get_all(self, session):
        boards = await session.execute(
            select(Board)
        )
        boards = boards.scalars().all()
        return boards


    async def create(self, session, board_data):
        new_board = Board(**board_data)
        session.add(new_board)
        await session.commit()
        await session.refresh(new_board)
        return new_board


    async def update(self, session, board_id, board_data):
        board_to_update = await self.get_by_id(session, board_id)

        for key, value in board_data.items():
            setattr(board_to_update, key, value)

        session.add(board_to_update)
        await session.commit()
        await session.refresh(board_to_update)

        return board_to_update

    async def delete(self, session, board_id):
        board_to_delete = await self.get_by_id(session, board_id)

        await session.delete(board_to_delete)
        await session.commit()

        return {"message": "Board deleted successfully"}
