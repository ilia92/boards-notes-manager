from abc import ABC, abstractmethod


class BaseRepo(ABC):
    @abstractmethod
    async def get_by_id(self, session, object_id: int):
        raise NotImplementedError

    @abstractmethod
    async def get_all(self, session):
        raise NotImplementedError

    @abstractmethod
    async def create(self, session, object_data):
        raise NotImplementedError

    @abstractmethod
    async def update(self, session, object_id, object_data):
        raise NotImplementedError

    @abstractmethod
    async def delete(self, session, object_id):
        raise NotImplementedError
