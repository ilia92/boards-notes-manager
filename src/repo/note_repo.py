from abc import ABC
from fastapi import HTTPException

from sqlalchemy import select

from models.note import Note
from repo.base_repo import BaseRepo


class NoteRepo(BaseRepo, ABC):
    async def get_by_id(self, session, note_id: int, check_flag: bool = False) -> Note:
        note = await session.execute(
            select(Note).filter(
                Note.id == note_id
            )
        )
        note = note.scalars().first()

        if not note:
            raise HTTPException(status_code=404, detail="Note not found")

        if not check_flag:
            note.seen += 1
            session.add(note)
            await session.commit()
            await session.refresh(note)

        return note

    async def get_all(self, session) -> list[Note]:
        notes = await session.execute(
            select(Note)
        )
        notes = notes.scalars().all()
        return notes

    async def create(self, session, note_data) -> Note:
        new_note = Note(**note_data)
        session.add(new_note)
        await session.commit()
        await session.refresh(new_note)
        return new_note

    async def update(self, session, note_id, note_data) -> Note:
        note_to_update = await self.get_by_id(session, note_id, check_flag=True)

        for key, value in note_data.items():
            setattr(note_to_update, key, value)

        session.add(note_to_update)
        await session.commit()
        await session.refresh(note_to_update)

        return note_to_update

    async def delete(self, session, note_id) -> dict:
        note_to_delete = await self.get_by_id(session, note_id)

        await session.delete(note_to_delete)
        await session.commit()

        return {"message": "Note deleted successfully"}
